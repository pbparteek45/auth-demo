import { Injectable } from '@angular/core';
import { AuthGuard } from './auth-guard.service';

@Injectable()
export class LoginGuardService extends AuthGuard {

  login(){
    if(this.authService.currentUser.admin)
    this.router.navigate(['/admin']);
  }
  
}
